#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo;

//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas

typedef struct carro{
    int num_llantas;
    char *color;
    char *marca;
    float costo;

}carrito;

//Constructor
void crear_carro(void *ref, size_t tamano);

//Destructor
void destruir_carro(void *ref, size_t tamano);

typedef struct estudiante{  // Definimos la estructura estudiante
    int  edad;
    char *nombre;
    char *apellido;
    char *telefono;

}estudiante;

//Crea la estructura estudiante
void crear_estudiante(void *ref, size_t tamano);

//Destructor
void destruir_estudiante(void *ref, size_t tamano);

typedef struct animal{  // Definimos la estructura animal
    int  numpatas;
    int  edad;
    char *raza;
    char *especie;
    char *nombre;

}animalito;


//Crea la estructura estudiante
void crear_animal(void *ref, size_t tamano);

//Destructor
void destruir_animal(void *ref, size_t tamano);
