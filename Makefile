
stact=ar rcs
ALL: estatico dinamico
dinamico: 
		gcc -shared -fPIC -o include/libslballoc.so src/Ejemplo.c src/slaballoc.c -I include/
		gcc -o bin/dinamico -I include/ src/prueba.c include/libslballoc.so -lm
estatico:  obj/Ejemplo.o obj/prueba.o  obj/slaballoc.o
		$(stact) include/libslballoc.a obj/Ejemplo.o obj/slaballoc.o
		gcc -static   obj/prueba.o include/libslballoc.a -lm -o bin/estatico

obj/prueba.o: src/prueba.c 
	gcc -Wall -c -Iinclude/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o
obj/slaballoc.o: src/slaballoc.c
	gcc -Wall -c -Iinclude/ src/slaballoc.c -o obj/slaballoc.o
#agregue las reglas que necesite


.PHONY: clean
clean:
	rm bin/* obj/*

